/*
 * main.cpp
 *
 *  Created on: Mar 5, 2010
 *      Author: ssmethurst
 */
#include <WProgram.h>
//没有这句话的情况下，会出现什么纯虚函数错误
//libArduinoCore.a(Print.o):(.data+0x6): undefined reference to `__cxa_pure_virtual'
extern "C" void __cxa_pure_virtual() {}

int dp=3;
int g=9;
int f=8;
int e=11;
int d=10;
int c=4;
int b=5;
int a=6;

int beepPin=7;

int incomingByte;



// 0
void number_0(){
  digitalWrite(a,LOW);
  digitalWrite(b,LOW);
  digitalWrite(c,LOW);
  digitalWrite(d,LOW);
  digitalWrite(e,LOW);
  digitalWrite(f,LOW);
  digitalWrite(g,HIGH);

}

// 1
void number_1(){
  digitalWrite(a,HIGH);
  digitalWrite(b,LOW);
  digitalWrite(c,LOW);
  digitalWrite(d,HIGH);
  digitalWrite(e,HIGH);
  digitalWrite(f,HIGH);
  digitalWrite(g,HIGH);

}

// 2
void number_2(){
  digitalWrite(a,LOW);
  digitalWrite(b,LOW);
  digitalWrite(c,HIGH);
  digitalWrite(d,LOW);
  digitalWrite(e,LOW);
  digitalWrite(f,HIGH);
  digitalWrite(g,LOW);

}

// 3
void number_3(){
  digitalWrite(a,LOW);
  digitalWrite(b,LOW);
  digitalWrite(c,LOW);
  digitalWrite(d,LOW);
  digitalWrite(e,HIGH);
  digitalWrite(f,HIGH);
  digitalWrite(g,LOW);

}

// 4
void number_4(){
  digitalWrite(a,HIGH);
  digitalWrite(b,LOW);
  digitalWrite(c,LOW);
  digitalWrite(d,HIGH);
  digitalWrite(e,HIGH);
  digitalWrite(f,LOW);
  digitalWrite(g,LOW);

}

// 5
void number_5(){
  digitalWrite(a,LOW);
  digitalWrite(b,HIGH);
  digitalWrite(c,LOW);
  digitalWrite(d,LOW);
  digitalWrite(e,HIGH);
  digitalWrite(f,LOW);
  digitalWrite(g,LOW);

}

// 6
void number_6(){
  digitalWrite(a,LOW);
  digitalWrite(b,HIGH);
  digitalWrite(c,LOW);
  digitalWrite(d,LOW);
  digitalWrite(e,LOW);
  digitalWrite(f,LOW);
  digitalWrite(g,LOW);

}

// 7
void number_7(){
  digitalWrite(a,LOW);
  digitalWrite(b,LOW);
  digitalWrite(c,LOW);
  digitalWrite(d,HIGH);
  digitalWrite(e,HIGH);
  digitalWrite(f,HIGH);
  digitalWrite(g,HIGH);

}

// 8
void number_8(){
  digitalWrite(a,LOW);
  digitalWrite(b,LOW);
  digitalWrite(c,LOW);
  digitalWrite(d,LOW);
  digitalWrite(e,LOW);
  digitalWrite(f,LOW);
  digitalWrite(g,LOW);

}

// 9
void number_9(){
  digitalWrite(a,LOW);
  digitalWrite(b,LOW);
  digitalWrite(c,LOW);
  digitalWrite(d,LOW);
  digitalWrite(e,HIGH);
  digitalWrite(f,LOW);
  digitalWrite(g,LOW);

}

void number_a(){
  digitalWrite(a,LOW);
  digitalWrite(b,LOW);
  digitalWrite(c,LOW);
  digitalWrite(d,LOW);
  digitalWrite(e,LOW);
  digitalWrite(f,HIGH);
  digitalWrite(g,LOW);
}

void number_b(){
  digitalWrite(a,HIGH);
  digitalWrite(b,HIGH);
  digitalWrite(c,LOW);
  digitalWrite(d,LOW);
  digitalWrite(e,LOW);
  digitalWrite(f,LOW);
  digitalWrite(g,LOW);
}

void number_c(){
  digitalWrite(a,LOW);
  digitalWrite(b,HIGH);
  digitalWrite(c,HIGH);
  digitalWrite(d,LOW);
  digitalWrite(e,LOW);
  digitalWrite(f,LOW);
  digitalWrite(g,HIGH);
}

void number_d(){
  digitalWrite(a,HIGH);
  digitalWrite(b,LOW);
  digitalWrite(c,LOW);
  digitalWrite(d,LOW);
  digitalWrite(e,LOW);
  digitalWrite(f,HIGH);
  digitalWrite(g,LOW);
}

void number_e(){
  digitalWrite(a,LOW);
  digitalWrite(b,HIGH);
  digitalWrite(c,HIGH);
  digitalWrite(d,LOW);
  digitalWrite(e,LOW);
  digitalWrite(f,LOW);
  digitalWrite(g,LOW);
}

//
void number_null(){
  digitalWrite(a,HIGH);
  digitalWrite(b,HIGH);
  digitalWrite(c,HIGH);
  digitalWrite(d,HIGH);
  digitalWrite(e,HIGH);
  digitalWrite(f,HIGH);
  digitalWrite(g,HIGH);

}

// .
void dot(int a){
  if(a)
    digitalWrite(dp,LOW);
  else
    digitalWrite(dp,HIGH);
}


void beep_long(){
  digitalWrite(beepPin,HIGH);
  delay(80);
  digitalWrite(beepPin,LOW);
  delay(30);
}

void beep_short(){
  digitalWrite(beepPin,HIGH);
  delay(40);
  digitalWrite(beepPin,LOW);
  delay(30);
}

void beep_0(){
  for(int i=0;i<5;i++){
    beep_long();
  }

}

void beep_1(){

   beep_short();
  for(int i=0;i<4;i++){
    beep_long();
  }
}

void beep_2(){
  for(int i=0;i<2;i++){
    beep_short();
  }
  for(int i=0;i<3;i++){
    beep_long();
  }
}

void beep_3(){
  for(int i=0;i<3;i++){
    beep_short();
  }
  for(int i=0;i<2;i++){
    beep_long();
  }
}

void beep_4(){
  for(int i=0;i<4;i++){
    beep_short();
  }

    beep_long();

}
void beep_5(){
  for(int i=0;i<5;i++){
    beep_short();
  }

}

void beep_6(){
  beep_long();
  for(int i=0;i<4;i++){

     beep_short();
  }
}

void beep_7(){
  for(int i=0;i<2;i++){
    beep_long();
  }

  for(int i=0;i<3;i++){
    beep_short();
  }

}

void beep_8(){
  for(int i=0;i<3;i++){
    beep_long();
  }

  for(int i=0;i<2;i++){
    beep_short();
  }

}

void beep_9(){
  for(int i=0;i<4;i++){
    beep_long();
  }

    beep_short();


}

void beep_a(){
  beep_short();
  beep_long();


}

void beep_b(){
  beep_long();
  for(int i=0;i<3;i++){
    beep_short();

  }
}

void beep_c(){
  beep_long();
  beep_short();
  beep_long();
  beep_short();

}

void beep_d(){
  beep_long();
  for(int i=0;i<2;i++){
    beep_short();

  }
}

void beep_e(){
  beep_short();

}

void setup()
{
    pinMode(dp,OUTPUT);
    pinMode(g,OUTPUT);
    pinMode(f,OUTPUT);
    pinMode(e,OUTPUT);
    pinMode(d,OUTPUT);
    pinMode(c,OUTPUT);
    pinMode(b,OUTPUT);
    pinMode(a,OUTPUT);

    pinMode(beepPin,OUTPUT);
    Serial.begin(9600);

    number_null();
    dot(1);
    delay(200);
    dot(0);
    delay(300);
    dot(1);
    delay(200);
    dot(0);


    number_0();
    delay(30);
    number_null();
    delay(50);
    number_1();
    delay(30);
    number_null();
    delay(50);
    number_2();
    delay(30);
    number_null();
    delay(50);
    number_3();
    delay(30);
    number_null();
    delay(50);
    number_4();
    delay(30);
    number_null();
    delay(50);
    number_5();
    delay(30);
    number_null();
    delay(50);
    number_6();
    delay(30);
    number_null();
    delay(50);
    number_7();
    delay(30);
    number_null();
    delay(50);
    number_8();
    delay(30);
    number_null();
    delay(50);
    number_9();
    delay(30);
    number_null();
    delay(50);

}


void loop()
{
    if(Serial.available()>0){
       incomingByte=Serial.read();
      switch(incomingByte){
         case '1':
           number_null();
           delay(20);
           number_1();
           beep_1();
           break;
         case '2':
           number_null();
           delay(20);
           number_2();
           beep_2();
           break;
         case '3':
           number_null();
           delay(20);
           number_3();
           beep_3();
           break;
         case '4':
           number_null();
           delay(20);
           number_4();
           beep_4();
           break;
         case '5':
           number_null();
           delay(20);
           number_5();
           beep_5();
           break;
         case '6':
           number_null();
           delay(20);
           number_6();
           beep_6();
           break;
         case '7':
           number_null();
           delay(20);
           number_7();
           beep_7();
           break;

         case '8':
           number_null();
           delay(20);
           number_8();
           beep_8();
           break;
         case '9':
           number_null();
           delay(20);
           number_9();
           beep_9();
           break;
         case '0':
           number_null();
           delay(20);
           number_0();
           beep_0();
           break;
         case ' ':
           number_null();
           break;
         case 'a':
           delay(20);
           number_a();
           beep_a();
           break;
          case 'b':
           delay(20);
           number_b();
           beep_b();
           break;
         case 'c':
           delay(20);
           number_c();
           beep_c();
           break;
         case 'd':
           delay(20);
           number_d();
           beep_d();
           break;
         case 'e':
           delay(20);
           number_e();
           beep_e();
           break;

      }
    }

}



int main(void) {

  /* Must call init for arduino to work properly */
  init();
  setup();

  for (;;) {
	  loop();
  } // end for
} // end main
